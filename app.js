const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect(
  "mongodb+srv://admin:admin@batch139.kdoph.mongodb.net/signup",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => console.log("Connected to database"));

const signupSchema = new mongoose.Schema({
  username: String,
  password: String,
});

const signup = mongoose.model("signup", signupSchema);

app.post("/signup", (req, res) => {
  signup.findOne({ name: req.body.username }, (err, result) => {
    console.log(result);
    if (result != null && result.username == req.body.username) {
      return res.send("Username already used");
    } else {
      let newSignup = new signup({
        username: req.body.username,
        password: req.body.password,
      });

      newSignup.save((err, savedSignup) => {
        if (err) {
          return console.error(err);
        } else {
          return res.send("New user created");
        }
      });
    }
  });
});

app.get("/users", (req, res) => {
  signup.find({}, (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.listen(port);
